let nodemailer = require('nodemailer');

let mailerConfig = {
    host: "smtp.strato.de",
    port: 587,
    auth: {
        user: "contact@helopha.com",
        pass: "password123!@#."
    }
};
let transporter = nodemailer.createTransport(mailerConfig);

const sendMail = (data) => {
    console.log(data);
    let mailOptions = {
        from: mailerConfig.auth.user,
        to: 'service@helopha.com',
        subject: 'Contact',
        html: `<body>` +
            `<p> Company: ` + data.company + `</p>` +
            `<p> Name: ` + data.name + `</p>` +
            `<p> Email: ` + data.email + `</p>` +
            `<p> Phone: ` + data.phone + `</p>` +
            `<p> Message: ` + data.message + `</p>` +
            `</body>`
    };
    
    transporter.sendMail(mailOptions, function (error) {
        if (error) {
            console.log('error:', error);
        } else {
            console.log('good');
        }
    });
}
module.exports = sendMail;
