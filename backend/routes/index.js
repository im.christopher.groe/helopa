var express = require('express');
const sendMail = require('../utils/sendMail');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/sendmail', function(req, res, next) {
  try{
    sendMail(req.body);

    res.status(200).json({
      status: 'success',
    });
  } catch (err) {
    next(err);
  }
});

module.exports = router;
